# Modelling and Pricing Air Pollution Derivatives

Data and code needed to replicate the analysis in the paper "Modelling and Pricing Air Air Pollution Derivatives"


### Links

The precursor to this work was first presented at the EcoSta2018 conference in Hong Kong. 
The presentation abstract can be found here: [Stochastic modelling of ambient air quality and pricing of air pollution derivatives](http://www.cmstatistics.org/RegistrationsV2/EcoSta2018/viewSubmission.php?in=705&token=pss947433po7291nq07916ps5586pn7r)
